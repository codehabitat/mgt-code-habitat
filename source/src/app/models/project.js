'use strict'

const mongoose = require('mongoose')
const mongoCLient = require('./../fn/mongooseClient')

const Schema = mongoose.Schema

const ProjectSchema = new Schema({
  name: { type: String },
  slug: { type: String },
})

module.exports = mongoCLient.model('Project', ProjectSchema)
