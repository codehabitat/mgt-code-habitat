'use strict'

const {
  makeExecutableSchema,
  // addMockFunctionsToSchema,
} = require('graphql-tools')

const requireGraphql = require('./../fn/requireGraphqlFile')
const path = require('path')

const resolvers = require('./resolvers')
const typeDefs = requireGraphql(path.join(__dirname, 'schema.gql'))

const schema = makeExecutableSchema({ typeDefs, resolvers })

// const mocks = require('./mocks')
// addMockFunctionsToSchema({ schema, mocks })

module.exports = schema
