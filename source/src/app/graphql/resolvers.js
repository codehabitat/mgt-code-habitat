'use strict'

const { Project } = require('./connectors')

const resolvers = {
  Query: {
    // author(root, args) {
    //   return Author.find({ where: args })
    //   // return { id: 1, firstName: 'Hello', lastName: 'Worasdasdsadld' }
    // },
    // allAuthors() {
    //   return Author.getAll()
    //   // return [{ id: 1, firstName: 'Hello', lastName: 'World' }]
    // },
    listProjects(root, args) {
      return Project.find({})
    },
  },
  // Author: {
  //   posts(author) {
  //     return Author.getPosts()
  //     // return [
  //     //     { id: 1, title: 'A post', text: 'Some text', views: 2 },
  //     //     { id: 2, title: 'Another post', text: 'Some other text', views: 200 }
  //     // ]
  //   },
  // },
  // Post: {
  //   author(post) {
  //     return Post.getAuthor()
  //     // return { id: 1, firstName: 'Hello', lastName: 'World' }
  //   },
  // },
  Mutation: {
    addProject: (root, args) => {
      return Project.create(args.input)
    },
  },
}
module.exports = resolvers
