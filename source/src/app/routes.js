'use strict'

const { graphqlExpress, graphiqlExpress } = require('apollo-server-express')
const bodyParser = require('body-parser')
const mainController = require('./controllers/mainController')
const graphqlController = require('./controllers/graphqlController')

module.exports = app => {
  // Status endpoint
  app.get('/', mainController.index)
  // Graphql endpoint
  app.use('/graphql', bodyParser.json(), graphqlExpress(graphqlController.index))
  // if you want GraphiQL enabled
  app.get('/graphiql', graphiqlExpress(graphqlController.dev))
}
