'use strict'

var myGraphQLSchema = require('./../graphql')

module.exports = {
  index: { schema: myGraphQLSchema },
  dev: { endpointURL: '/graphql' },
}
