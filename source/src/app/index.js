'use strict'

// require('./fn/debugUtils').init()

require('./fn/mongooseClient')

const app = require('express')()

require('./bootstrap')(app)

module.exports = app