'use strict'

// Includes
// const mainSchema = require('./../schemas/mainSchema')
const { request, utils, dbConnectionReady } = require('./../utils/')

describe('Main', function() {
  beforeAll(() => {
    this.app = require('../server/app').app
    return dbConnectionReady()
  })

  beforeEach(() => {
    this.request = request(this.app)
  })

  it(
    utils.define({
      method: 'get',
      url: '/',
      info: 'response ok',
    }),
    async () => {
      // Request
      await this.request.send({ method: 'get', url: '/' })
      // Assertions
      expect(this.request.response.body).toBe('hi')
    }
  )

  it(
    utils.define({
      method: 'get',
      url: '/not-found',

      info: 'response ok',
    }),
    async () => {
      // Request
      await this.request.send({ method: 'get', url: '/not-found' })
      // Assertions
      expect(this.request.response.body).toBe('Not found')
    }
  )
})
