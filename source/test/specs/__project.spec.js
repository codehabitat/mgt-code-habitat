'use strict'

// Includes
// const mainSchema = require('./../schemas/mainSchema')
const { request, utils, dbConnectionReady } = require('./../utils/')

describe('Hello', function() {
  beforeAll(() => {
    this.app = require('../server/app').app
    return dbConnectionReady()
  })

  beforeEach(() => {
    this.request = request(this.app)
  })

  it(
    utils.define({
      method: 'post',
      url: '/graphql',
      info: 'listProjects ok',
    }),
    async () => {
      // Request
      await this.request.send({
        method: 'post',
        url: '/graphql',
        body: {
          query: `
                {
                    listProjects {
                        name
                        slug
                    }
                }
            `,
        },
      })
      // Assertions
      expect(this.request.response.body.data).toEqual({ listProjects: [] })
    }
  )

  it(
    utils.define({
      method: 'post',
      url: '/graphql',
      info: 'addProject ok',
    }),
    async () => {
      // Request
      await this.request.send({
        method: 'post',
        url: '/graphql',
        body: {
          query: `
                mutation {
                addProject(input: {
                    name: "api",
                    slug: "api"
                }) {
                    name
                    slug
                }
                }
            `,
        },
      })
      // Assertions
      expect(this.request.response.body.data).toEqual({ addProject: { name: 'api', slug: 'api' } })
    }
  )

  it(
    utils.define({
      method: 'post',
      url: '/graphql',
      info: 'listProjects ok',
    }),
    async () => {
      // Request
      await this.request.send({
        method: 'post',
        url: '/graphql',
        body: {
          query: `
                {
                    listProjects {
                        name
                        slug
                    }
                }
            `,
        },
      })
      // Assertions

      expect(this.request.response.body.data).toEqual({
        listProjects: [{ name: 'api', slug: 'api' }],
      })
    }
  )
})
