'use strict'

// const _ = require('lodash')
// const util = require('util')
const mock = require('mock-require')

/**
 * Mock express
 */

let express = require('express')
if (process.env.LISTEN !== 1) {
  express.application.listen = function() {
    return null
  }
}
mock('express', express)

/**
 * Mock morgan
 */
// if (process.env.LOGS != 1) {
//     debug.info('invalidate-morgan')
//     mock('morgan', function () {
//         return function (req, res, next) {
//             next()
//         }
//     })
// }

/**
 * console log
 */

// /**
//  * Use util.inspect to print a pretty object
//  * @param  {Mixed} obj
//  * @return {void}
//  */
// function inspect(obj) {
//     return util.inspect(obj, {
//         depth: 30,
//         colors: true
//     })
// }

// console.inspect = function () {
//     console.log.apply(console, Array.prototype.slice.call(arguments).map(inspect))
// }

// /**
//  * util.inspect improvements
//  */

// //Define colors
// util.inspect.styles = {
//     special: 'cyan',
//     number: 'green',
//     boolean: 'green',
//     undefined: 'cyan',
//     null: 'cyan',
//     string: 'yellow',
//     symbol: 'magenta',
//     date: 'magenta',
//     regexp: 'green',
//     name: 'bold'
// }
