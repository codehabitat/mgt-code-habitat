'use strict'

let chai = require('chai')
chai.use(require('chai-json-schema'))
// let expect = chai.expect

// const uuidV1 = require('uuid/v1')
// const errorSchema = require('./../schemas/errorSchema')

class BasicRequestTest {
  constructor(app) {
    this.app = app
    this.response = null
    this.request = null
  }

  async send(opts) {
    let request = require('supertest').agent(this.app)
    // Get parameters of request
    let method = (opts && opts.method) || 'get'
    let url = (opts && opts.url) || '/'
    let body = (opts && opts.body) || null
    let fields = (opts && opts.fields) || null
    let bearer = Buffer.from('this.crm').toString('base64')
    let pass = 'XXXXXXXXXXXXXXXXX'
    let key = Buffer.from(pass).toString('base64')
    let req = request[method](url).set('authorization', `Bearer ${bearer}`)

    // Overwride request anonymous
    // if (opts && opts.anon == true)
    // req = request[method](url)

    // Set language in headers
    req.set('x-user-language', 'es_ES')

    if (opts.lock !== false)
      if (opts.lock) req.set('x-lock-key', opts.lock + '')
      else req.set('x-lock-key', key)

    if (opts.buffer === true) {
      // req
      //     .parse(binaryParser)
      //     .buffer()
    }

    if (body) req.send(body)

    if (fields) {
      fields.forEach(f => {
        req.field(f[0], f[1])
      })
    }

    return (
      req
        // .expect('Content-type',/json/)
        .then(response => {
          this.response = response
          return response
        })
        .catch(err => {
          // console.log(err)
          if (!this.response) this.response = {}
          this.response.body = err.body
          this.response.statusCode = err.status || 404
          // console.log(err)
          return err.body
        })
    )
  }

  // validate(opts) {

  //     let statusCode = opts && opts.statusCode || 200
  //     let status = opts && opts.status || true
  //     let schema = opts && opts.schema || {}

  //     expect(this.response.statusCode).to.be.equal(statusCode)
  //     expect(this.response.body.status).to.be.equal(status)
  //     expect(this.response.body.data).to.be.jsonSchema(schema)

  //     return true
  // }

  // validateError(opts) {
  //     let statusCode = opts && opts.statusCode ? opts.statusCode : 200
  //     let status = opts.status || false
  //     let schema = opts && opts.schema ? opts.schema : errorSchema
  //     let code = opts.code

  //     expect(this.response.statusCode).to.be.equal(statusCode)
  //     expect(this.response.body.status).to.be.equal(status)
  //     expect(this.response.body.error).to.be.jsonSchema(schema)
  //     expect(this.response.body.error.code).to.be.equal(code)

  //     return true
  // }
}

module.exports = BasicRequestTest
