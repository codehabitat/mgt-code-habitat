'use strict'

process.setMaxListeners(0)

const BasicRequestTest = require('./BasicRequestTest')

module.exports = {
    request: function name(app) {
        return new BasicRequestTest(app)
    },
    utils: require('./utils'),
    dbConnectionReady: require('./dbConnectionReady'),
    // fixtureGenerator: require('./promoGenerator')
}