'use strict'

const chalk = require('chalk')

module.exports = {
    define(obj) {
        return module.exports['_' + obj.method]() + ' ' + chalk.bold(obj.url) + ' \n    ' + '- ' + obj.info
    },
    _get() {
        return chalk.bgBlue(' GET ')
    },
    _post() {
        return chalk.bgYellow(' POST ')
    },
    _put() {
        return chalk.bgMagenta(' PUT ')
    },
    _delete() {
        return chalk.bgRed(' DELETE ')
    },
    _unit() {
        return chalk.bgGreen(' UNIT ')
    },
}