'use strict'

const mongooseClient = require('./../../src/app/fn/mongooseClient')

module.exports = () => {
  if (mongooseClient.readyState === 1) return Promise.resolve()

  return new Promise(function(resolve) {
    mongooseClient.on('open', result => {
      // mongooseClient.db.unref()
      resolve()
    })
  })
}
