'use strict'

process.setMaxListeners(0)

jest.mock('./../../src/parameters.json', () => {
  let db = Date.now()
  let parameters = Object.assign({}, require('../extra/parameters.json'))
  parameters.mongodbConnectionUri = parameters.mongodbConnectionUri + '-' + db
  return parameters
})

beforeAll(() => {
  return Promise.resolve()
})

afterAll(() => {
  let mongooseClient = require('./../../src/app/fn/mongooseClient')
  return new Promise(function(resolve) {
    mongooseClient.close(function() {
      resolve()
    })
  })
})
