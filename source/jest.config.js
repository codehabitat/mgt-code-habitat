'use strict'

module.exports = {
  globalSetup: './test/utils/deleteDatabase.js',
  globalTeardown: './test/utils/tearDown.js',
  setupTestFrameworkScriptFile: './test/utils/setup.js',
  testEnvironment: 'node',
  coverageDirectory: 'reports',
  coverageReporters: ['json', 'lcov', 'html'],
  coveragePathIgnorePatterns: ['node_modules', 'test/server', 'test/utils'],
}
